# SERUM Tool Configs

This repository gathers several config files for different tools that are used during development and/or in our CI/CD pipelines.

Currently, we have config files for following tools:

- [cargo-deny](https://github.com/EmbarkStudios/cargo-deny)
    - Used for checking and listing licenses.
- [cSpell](https://www.npmjs.com/package/cspell)
    - Used for spell checking code and documentation.

Other used tooling doesn't require specific config files.

## Used Templates

The cSpell configuration `cspell.json` is based on cSpell settings as described in <https://www.npmjs.com/package/cspell> chapter "Customization". It uses the dictionary file `serum-dictionary.txt` as an allow-list of unknown words. Since cSpell was originally developed as vscode extension, you can use the `serum-dictionary.txt` in vscode.

The cargo-deny configuration `deny.toml` was generated with the command `cargo deny init`. The generated template was then adjusted to our needs.

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>

